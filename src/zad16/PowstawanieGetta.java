package zad16;

import java.util.Arrays;
import java.util.Random;

/**
 * @author Kaczorkiewicz
 * 2021_06_24
 * 
 * Zadanie 16
 * 
 * Proszę spróbować zrealizować (znów, na początek przy pomocy tablicy znakowej) 
 * algorytm ilustrujący powstawanie getta (alg . Schellinga).
 * 
 * Model segregacji rasowej
 * ●Pomimo jego prostocie wyniki nie są zupełnie trywialne
 * ●Mamy grupę osobników, którzy przenoszą się, jeżeli procent sąsiadów innej rasy 
 * w najbliższym sąsiedztwie przekracza 30%
 * ●W tym modelu równomiernie wymieszane społeczeństwo rozwarstwia się 
 * na czyste etnicznie grupy
 * Jednostki żyją na dwuwymiarowej planszy. W każdej komórce może być najwyżej 
 * jeden osobnik. Każdy osobnik należy do jednej z dwóch ras. 
 * ●W każdym kroku osobnik wybiera kierunek, w którym patrzy (płn, płd, wsch, zach). 
 * Jeżeli na daną pustą komórkę patrzy dokładnie jeden osobnik, może tam się przenieść. 
 * ●Przenosi się wtedy i tylko wtedy, gdy stosunek liczby osobników innej rasy 
 * do liczby wszystkich osobnikóww jego okolicy (9 komórek) jest większy niż t. 
 * ●Biały – puste komórki, niebieski, czerwony – dwie rasy
 * 
 * na podstawie: http://nifty.stanford.edu/2014/mccown-schelling-model-segregation/
 * (dostępność na dzień 2021_06_24) 
 * 
 * Założenia:
 * - tablica[10][10] to osobniki
 * - kierunekPatrzenia[10][10] pilnuje, gdzie patrzą się osobnicy
 * - współczynnik t pilnuje nam tolerancję na osobników innej rasy
 * - W - jedna rasa, 
 * - B - druga rasa, 
 * - . - pusta komórka
 * - losujemy stan początkowy z przedziału 30-70 osobników, jeśli komórka jest już zajęta,
 * to losujemy jeszcze raz
 * - czy nie przydałaby nam się struktura pilnująca zawartości, zawierająca także informację
 * o kierunku, w którym się patrzy dany osobnik?
 * 
 * metoda druckTable() wypisuje tablicę na ekranie
 * metoda startTable() generuje startowe wypełnienie tablicy osobnikami
 * metoda copyTable(String[][], String[][]) pilnuje kopiowania tablic 
 * 
 */
public class PowstawanieGetta {
    private String[][] tablica;
    private String[][] kierunekPatrzenia;
    private int t;
    
    public PowstawanieGetta() {
        tablica = new String[10][10];
        kierunekPatrzenia = new String[10][10];
        t = 3;
        
        for(int i = 0; i < tablica.length; i++){
            for(int j = 0; j < tablica[i].length; j++){
                tablica[i][j] = ".";
            }
        }
        for(int i = 0; i < kierunekPatrzenia.length; i++){
            for(int j = 0; j < kierunekPatrzenia[i].length; j++){
                kierunekPatrzenia[i][j] = ".";
            }
        }
    }
    
    public void setT(int t){
        this.t = t;
    }
    
    public int getT(){
        return t;
    }
    
    
    public void copyTable(String[][] t1, String[][] t2){
        for (int i = 0; i < t1.length; i++) {
            t2[i] = Arrays.copyOf(t1[i], t1[i].length);
        }
    }
    
    
    public void druckTable(){
        for(int i = 0; i < tablica.length; i++){
            for(int j = 0; j < tablica[i].length; j++){
                System.out.print(tablica[i][j]);
            }
            System.out.println();
        }
    }
    
    public int countPeople(){
        int wynik = 0;
        for(int i = 0; i < tablica.length; i++){
            for(int j = 0; j < tablica[i].length; j++){
                if(!tablica[i][j].equals(".")){
                    wynik++;
                }
            }
        }
        return wynik;
    }
    
    
    public void startTable(){
        Random r = new Random();
        
        int murzyni = r.nextInt(20) + 15;
        int biali = r.nextInt(20) + 15;
        System.out.println("Wylosowano: " + (murzyni + biali) + " ludzi.");
        int iteracje = 0;
        while(iteracje != murzyni+biali){
            int w = r.nextInt(10);
            int k = r.nextInt(10);
            if(tablica[w][k].equals(".")){
                if(murzyni > 0){
                    tablica[w][k] = "B";
                    murzyni--;
                } else {
                    tablica[w][k] = "W";
                    biali--;
                }
            }
        }
    }
    
    
    
    
     public static void main(String[] args) {
         System.out.println("Powstawanie getta - początek.");
         PowstawanieGetta getto = new PowstawanieGetta();
         getto.startTable();
         getto.druckTable();
         
         System.out.println("Koniec powstawania getta.");
     }
}
