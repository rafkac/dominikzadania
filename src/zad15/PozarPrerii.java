package zad15;

import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author Kaczorkiewicz
 * 2021_06_23
 * 
 * Zadanie 15
 * Proszę spróbować zrealizować (znów, na początek przy pomocy tablicy znakowej) 
 * algorytm 'pożar prerii'. Proszę zwrócić uwagę że jest on wykorzystywany np. 
 * w programie Paint do wypełniania kolorem zamkniętego obszaru. 
 * W realizacji wygodne jest wykorzystanie stosu do gromadzenia wykazu 
 * już sprawdzonych pikseli.
 * 
 * Analogicznie jak w zadaniu wcześniejszym, "puste" pola będą oznaczone kropką, a
 * "płonące" przez "O". 
 * Pożar rozchodzi się w każdym kierunku (nie uwzględniamy wiatru) z taką samą prędkością, 
 * aż osiągnie koniec przestrzeni.
 * Po każdym cyklu pożaru wyświetlimy stan.
 * 
 * Dodajemy tablicę dwuwymiarową - lepiej oddaje płaszczyznę, więc łatwiej będzie się 
 * nam odwoływać do sąsiadów zapalonej komórki.
 * 
 * Potrzebujemy metody openFire(), losującej punkt startowy pożaru.
 * Metoda changeState() zmienia stan punktu z niepalącego się na palący się
 * metoda piszTablice() wypisuje na ekranie zawartość tablicy
 * metoda checkPlace() zwraca true, jeśli komórka powinna zapłonąć (ma pożar w sąsiedztwie)
 * oraz false, jeśli nie ma pożaru w sąsiedztwie
 * 
 * metoda fire1() zrobi pożar iteracyjnie, wyświetlając zapalony obszar po każdej iteracji
 * metoda fire2() zrobi pożar rekurencyjnie, wyświetlając zmiany z drobnym opóźnieniem
 * metoda fire3() zrobi pożar z wykorzystaniem stosu
 * 
 * Każda z powyższych metod występuje także w wersji dla macierzy kwadratowej 10x10, 
 * różnią się dodaniem "Kw" na końcu nazwy.
 * Konstruktorem inicjujemy obie tablice, więc możemy pracować na obu jednocześnie 
 * (i na przykład porównywać działanie).
 * 
 * Uwaga - zauważmy, że klonowanie tablicy dwuwymiarowej działa słabo (przynajmniej 
 * przy inicjalizacji tablicy, bo później juz normalnie), dlatego kopiujemy tablicę "ręcznie".
 * 
 * 
 */
public class PozarPrerii {
    private String[] tablica;
    private String[][] tabKw;
    
    
    public PozarPrerii(){
        tablica = new String[100];
        tabKw = new String[10][10];
        for(int i = 0; i < tablica.length; i++){
            tablica[i] = ".";
        }
        for(int i = 0; i < tabKw.length; i++){
            for(int j = 0; j < tabKw[i].length; j++){
                tabKw[i][j] = ".";
            }
        }
    }
    
    
    public void openFire(){
        Random r = new Random();
        int l = r.nextInt(100);
        changeState(l);
    }
    
    public void openFireKw(){
        Random r = new Random();
        int w = r.nextInt(10);
        int k = r.nextInt(10);
        changeStateKw(w,k);
        System.out.println("Wylosowane liczby to: " + w + " i " + k);
    }
    
    
    public void changeState(int k){
        tablica[k] = "O";
    }
    
    public void changeStateKw( int i, int j){
        tabKw[i][j] = "O";
        //System.out.println("tabKw: " + tabKw[i][j]);
    }
    
    
    public void piszTablice(){
        for(int i =  0; i < tablica.length; i++){
            System.out.print(tablica[i]);
            if( (i+1)%10 == 0){
                System.out.println();
            }
        }
    }
    
    public void piszTabliceKw(){
        for(int i = 0; i < tabKw.length; i++){
            for(int j = 0; j < tabKw[i].length; j++){
                System.out.print(tabKw[i][j]);
            }
            System.out.println();
        }
    }
    
    // uwzględniamy ośmiu sąsiadów
    public void fire1(){
        //String[] pomoc = tablica.clone();
        String[] pomoc = new String[tablica.length];
        for(int i = 0; i < tabKw.length; i++) {
            pomoc[i] = tablica[i];
        }
        for(int i = 0; i < tablica.length; i++){
            if(pomoc[i].equals("O")){
                continue;
            }
            if(i+1 < tablica.length && tablica[i+1].equals("O")){
                pomoc[i] = "O";
                continue;
            }
            if(i-1 >= 0 && tablica[i-1].equals("O")){
                pomoc[i] = "O";
                continue;
            }
            if(i+10 < tablica.length && tablica[i+10].equals("O")){
                pomoc[i] = "O";
                continue;
            }
            if(i+11 < tablica.length && tablica[i+11].equals("O")){
                pomoc[i] = "O";
                continue;
            }
            if(i+9 < tablica.length && tablica[i+9].equals("O")){
                pomoc[i] = "O";
                continue;
            }
            if(i-11 >= 0 && tablica[i-11].equals("O")){
                pomoc[i] = "O";
                continue;
            }
            if(i-10 >= 0 && tablica[i-10].equals("O")){
                pomoc[i] = "O";
                continue;
            }
            if(i-9 > 0 && tablica[i-9].equals("O")){
                pomoc[i] = "O";
                continue;
            }
            
        }
        tablica = pomoc.clone();
    }
    
    // w wersji, że mamy tylko czterech sąsiadów, a nie ośmiu
    public void fire1Kw(){
        //String[][] pom = tabKw.clone();
        String[][] pom = new String[tabKw.length][];
        for (int i = 0; i < tabKw.length; i++) {
            pom[i] = Arrays.copyOf(tabKw[i], tabKw[i].length);
        }
        for(int i = 0; i < tabKw.length; i++){
            for(int j = 0; j < tabKw[i].length; j++){
                if(tabKw[i][j].equals("O")){
                    //System.out.println("Mamy ogień dla: tab["+i+"][" + j + "]");
                    //System.out.println("ogień - idziemy do kolejnej iteracji.");
                    if(i - 1 >= 0 ){
                        pom[i-1][j] = "O";
                        //System.out.println("Malujemy: " + (i-1) + " " + j);
                    }
                    if(i + 1 < pom.length){
                        pom[i+1][j] = "O";
                        //System.out.println("Malujemy: " + (i+1) + " " + j);
                    }
                    if(j - 1 >= 0){
                        pom[i][j-1] = "O";
                        //System.out.println("Malujemy: " + (i) + " " + (j-1));
                    }
                    if(j + 1 < pom[i].length){
                        pom[i][j + 1] = "O";
                        //System.out.println("Malujemy: " + i + " " + (j+1));
                    }
                //    continue; 
                }/*
                if(i - 1 >= 0 ){
                    if(tabKw[i-1][j].equals("O")){
                       pom[i][j] = "O"; 
                       continue; 
                    }
                }
                if(i + 1 < pom.length){
                   if(tabKw[i+1][j].equals("O")){
                       pom[i][j] = "O";
                       continue; 
                    }
                }
                if(j - 1 >= 0 ){
                   if(tabKw[i][j - 1].equals("O")){
                       pom[i][j] = "O";
                       continue; 
                    }
                }
                if(j + 1 < pom[i].length){
                   if(tabKw[i][j+1].equals("O")){
                       pom[i][j] = "O"; 
                       continue; 
                    }
                }*/
            }
        }
        tabKw = pom.clone();
    }
    
    
    public static void main(String[] args) {
        System.out.println("Pożar prerii - poczatek.");
        int iteracje = 7;
        
        PozarPrerii p = new PozarPrerii();
        /*
        p.openFire();
        p.piszTablice();
        for(int j = 0; j < iteracje; j++){
            System.out.println("Iteracja: " + j);
            p.fire1();
            p.piszTablice();
        }*/
        
        p.openFireKw();
        p.piszTabliceKw();
        for(int licz = 0; licz < iteracje; licz++){
            System.out.println("Iteracja: " + licz);
            p.fire1Kw();
            p.piszTabliceKw();
        }
        
        System.out.println("\nPożar prerii - koniec.");
    }
}
