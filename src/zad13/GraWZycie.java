package zad13;

import java.util.Random;

/**
 *
 * @author user
 * 
 * Zadanie 14
 * 
 * Wykorzystując tablicę znakową (wypełnioną np spacjami) proszę napisać program 
 * realizujący algorytm Conwaya ('gra w życie').
 * Proszę przeczytać trochę przy okazji o automatach komórkowych i obejrzeć 
 * popularne realizacje tego programu. Proszę pamiętać o opóźnieniu (delay) 
 * żeby było coś widać na ekranie.
 * 
 * - kwadratowa siatka oczek
 * - każda komórka może być żywa lub martwa
 * - komórka ożywa, jeśli ma dokładnie trzech sąsiadów żywych
 * - kormórka umiera, jeśli ma czterech sąsiadów (z przeludnienia) 
 *      lub mniej niż dwoje (z samotności)
 * 
 * plan - mamy tablicę 10x10 elementów, zapisanych jako tablica jednowymiarowa o dłiugości 100.
 * 
 * metoda czyUmiera(numerIndeksu) sprawdza, czy komórka o danym indeksie umrze
 * metoda czyOzywa(numerIndeksu) sprawdza, czy komórka o danym indeksie ożywa 
 * metoda ileSasiadow(numerIndeksu) zwraca ilość sąsiadów dla danego indeksu
 * metoda piszTablice() drukuje nam zawartość tablicy na ekranie 
 * metoda losujStartowe() losuje nam startowe komórki, które będą "żywe" na początku działania algorytmu, conajmniej 10
 * metoda cykl() wykonuje pojedynczy cykl życia komórek, czyli pojedynczy cykl algorytmu Conwaya
 * 
 * UWAGA - sprawdzając komórkę, porównujemy z wcześniejszą iteracją, a nie ze stanem "w tej chwili", 
 * tzn jeśli sąsiad naszej komórki ożył w tej iteracji, to nie ma to znaczenia dla stanu naszej komórki.
 */
public class GraWZycie {
    String[] tablica;
    
    public GraWZycie(){
        tablica = new String[100];
        for(int i = 0; i < tablica.length; i++){
            tablica[i] = ".";
        }
    }
    
    public void piszTablice(){
        for(int i =  0; i < tablica.length; i++){
            System.out.print(tablica[i]);
            if( (i+1)%10 == 0){
                System.out.println();
            }
        }
    }
    
    
    public void cykl(){
        String[] tabPomocnicza = tablica.clone();
        for(int i = 0; i < tablica.length; i++ ){
            int j = ileSasiadow(i);
            if(tablica[i].equals(".")){
                if(j == 3){
                    tabPomocnicza[i] = "O";
                //    System.out.println("Komórka "+ i + " ożywa");
                }
            }
            else{
                if(j < 2 || j > 3){
                    tabPomocnicza[i] = ".";
                //    System.out.println("Komórka "+ i + " umiera");
                }
            }
        }
        tablica = tabPomocnicza.clone();
    }
    
    
    public void losujStartowe(){
        Random r = new Random();
        int losowana = r.nextInt(70) + 30;
        for(int i = 0; i < losowana; i++){
            int l = r.nextInt(100);
            tablica[l] = "O";
        }
        System.out.println("ilość żywych na początku: " + losowana);
    }
    
    
    public int ileSasiadow(int k){
        int wynik = 0;
       // System.out.println("Sprawdzamy komórkę: "+ k + " i sąsiadów: " + (k-10) 
       //         +" " + (k+10) +" " + (k-1) +" " + (k+1));
        if(k - 10 >= 0){
            if(!tablica[k-10].equals(".") ){
                wynik++;
             //   System.out.println("mamy życie: "+ (k-10));
            }
        }
        if(k + 10 < tablica.length){
            if(!tablica[k+10].equals(".") ){
                wynik++;
            //    System.out.println("mamy życie: "+ (k+10));
            }
        }
        if(k-1 >= 0){
            if(!tablica[k-1].equals(".") ){
                wynik++;
             //   System.out.println("mamy życie: "+ (k-1));
            }
        }
        if(k+1 < tablica.length){
            if(!tablica[k+1].equals(".") ){
                wynik++;
             //   System.out.println("mamy życie: "+ (k+1));
            }
        }
        // teraz dodajemy sprawdzanie sąsiadów, z którymi stykamy się rogami
        if(k - 11 >= 0){
            if(!tablica[k-11].equals(".") ){
                wynik++;
             //   System.out.println("mamy życie: "+ (k-11));
            }
        }
        if(k - 9 >= 0){
            if(!tablica[k-9].equals(".") ){
                wynik++;
             //   System.out.println("mamy życie: "+ (k-9));
            }
        }
        if(k + 11 < tablica.length ){
            if(!tablica[k+11].equals(".") ){
                wynik++;
             //   System.out.println("mamy życie: "+ (k+11));
            }
        }
        if(k + 9 < tablica.length){
            if(!tablica[k+9].equals(".") ){
                wynik++;
             //   System.out.println("mamy życie: "+ (k+9));
            }
        }
        return wynik;
    }
    
    
    public int ileZywych(){
        int wynik = 0;
        for(int i = 0; i < tablica.length; i++){
            if(tablica[i].equals("O")){
               wynik++; 
            }
        }
        return wynik;
    }
    
    
    public boolean czyUmiera(int k){
        boolean wynik = true;
        int i = 0;
        if(k-10 > 0 && !tablica[k-10].equals(".") ){
            i++;
        }
        if(k+10 > 0 && !tablica[k+10].equals(".") ){
            i++;
        }
        if(k-1 > 0 && !tablica[k-1].equals(".") ){
            i++;
        }
        if(k+1 > 0 && !tablica[k+1].equals(".") ){
            i++;
        }
        if(i < 2 || i == 4){
            wynik = true;
        }
        else{
            wynik = false;
        }
        
        return wynik;
    } 
    
    
    public static void main(String[] args) {
        System.out.println("Gra w zycie - poczatek.");
        
        GraWZycie pierwsza = new GraWZycie();
        pierwsza.losujStartowe();
        pierwsza.piszTablice();
        for(int t = 1; t < 10; t++){
            System.out.println("\nIteracja: " + t);
            pierwsza.cykl();
            pierwsza.piszTablice();
            System.out.println("\nŻywych: " + pierwsza.ileZywych());
        }
        
        System.out.println("\nGra w zycie - koniec.");
    }
}
